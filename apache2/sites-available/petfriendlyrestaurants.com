

<VirtualHost *:80 *:8041 dev.sqr-dev.pl:8041 >
#<VirtualHost *:80>
	ServerAdmin petfriendlyrestaurants.com@askmarcos.com
	ServerName pfr.sqr-dev.pl

    ServerAlias  *:8041
#    ServerAlias *.pfr.sqr-dev.pl
#    ServerAlias *.petfriendlyrestaurants.com.sqr-dev.pl
    ServerAlias petfriendlyrestaurants.com.sqr-dev.pl
#    ServerAlias *.petfriendlyrestaurants.sqr-dev.pl
    ServerAlias petfriendlyrestaurants.sqr-dev.pl
#    ServerAlias *.petfriendlyrestaurants.com
#    ServerAlias petfriendlyrestaurants.com


	DocumentRoot /home/master/www/petfriendlyrestaurants.com
	<Directory />
		Options FollowSymLinks
		AllowOverride None
	</Directory>
	<Directory /home/master/www/petfriendlyrestaurants.com>
		Options Indexes FollowSymLinks MultiViews
		AllowOverride All
		Order allow,deny
		allow from all
	</Directory>

	ErrorLog /home/master/www/petfriendlyrestaurants.com/error.log

	LogLevel warn

	CustomLog /home/master/www/petfriendlyrestaurants.com/access.log combined

</VirtualHost>
