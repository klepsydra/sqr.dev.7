


PassengerRuby /usr/local/rvm/gems/ruby-1.9.3-p547/wrappers/ruby


<VirtualHost *:80 *:8041 dev.sqr-dev.pl:8042 >
#<VirtualHost *:80>
	ServerAdmin rorsqr.petfriendlyrestaurants.com@askmarcos.com
#	ServerName pfr.sqr-dev.pl
#    ServerName rorsqr.sqr-dev.pl
	ServerName rorsqr.petfriendlyrestaurants.com

    ServerAlias  *:8042

    ServerAlias petfriendlyrestaurants.com
    ServerAlias www.petfriendlyrestaurants.com

    ServerAlias pfr.sqr-dev.pl
    ServerAlias *.pfr.sqr-dev.pl
    ServerAlias *.petfriendlyrestaurants.com.sqr-dev.pl
    ServerAlias petfriendlyrestaurants.com.sqr-dev.pl
    ServerAlias *.petfriendlyrestaurants.sqr-dev.pl
    ServerAlias petfriendlyrestaurants.sqr-dev.pl
    ServerAlias *.rorsqr.petfriendlyrestaurants.com
    ServerAlias rorsqr.petfriendlyrestaurants.com

   <Directory /home/master/www/rorsqr.petfriendlyrestaurants.com>
		Options Indexes FollowSymLinks MultiViews
		AllowOverride All
		Order allow,deny
		allow from all
	</Directory>


#http://nathanhoad.net/how-to-ruby-on-rails-ubuntu-apache-with-passenger
    RailsEnv development

#RailsEnv development
PassengerHighPerformance on
#|?DOCROOT=/home/user/domains/domain.name/public_html/rails/refinery_test_install/| 



	DocumentRoot /home/master/www/rorsqr.petfriendlyrestaurants.com/public
      <Directory /home/master/www/rorsqr.petfriendlyrestaurants.com/public>
         # This relaxes Apache security settings.
         AllowOverride all
         # MultiViews must be turned off.
         Options -MultiViews
         # Uncomment this if you're on Apache >= 2.4:
         #Require all granted
      </Directory>


	ErrorLog /home/master/www/rorsqr.petfriendlyrestaurants.com/error.log

	LogLevel warn

	CustomLog /home/master/www/rorsqr.petfriendlyrestaurants.com/access.log combined



<IfModule mod_rewrite.c>
#/rorsqr.petfriendlyrestaurants.com/refinery/entries

#sqrdev.55.lt has address 77.253.211.255

# http://www.petfriendlyrestaurants.com/maintenance.html

#SERVER_NAME = www.petfriendlyrestaurants.com

#RewriteEngine Off
RewriteEngine On
RewriteCond  %{SERVER_NAME} ^www\.petfriendlyrestaurants\.com
#RewriteCond %{REMOTE_ADDR} ^177\.253\.211\.255
##RewriteCond %{REMOTE_ADDR} !^77\.253\.211\.255
RewriteCond %{DOCUMENT_ROOT}/maintenance.html -f
#RewriteCond %{DOCUMENT_ROOT}/maintenance.enable -f
RewriteCond %{SCRIPT_FILENAME} !maintenance.html
RewriteRule ^.*$ /maintenance.html [R=503,L]
#ErrorDocument 503 /maintenance.html

#Header Set Cache-Control "max-age=0, no-store"

#[Thu Aug 28 10:00:56 2014] [alert] [client 157.55.39.92] /home/master/www/rorsqr.petfriendlyrestaurants.com/.htaccess: Invalid command 'Header', perhaps misspelled or defined by a module not included in the server configuration

</IfModule>

</VirtualHost>
