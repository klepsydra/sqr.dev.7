#!/bin/bash

# stores some vital stuff in repo, noting changes:
# https://bitbucket.org/sqrd/sysadmin/src
# sysadmin and backend development/repository maintenance
#
# Including:
# /etc
# /srv/*.sh
#
# Sample usage:
# time /etc/cron.daily/sysrepo.sh |tee -a /tmp/sysrepo.log &
# /root/_repos/sysadmin/stage/


# Set defaults for comments to repo commit
commentetc="OTHERS-7 #time 3m #comment "
commentsa="OTHERS-7 #time 2m  #comment Push any changes from /srv and /etc to remote repo"

#Overwrite with parameter $1 if exists
#[[ $1 ]] && (export commentetc=$1; commentsa=$1)  # not quite!

commentetc=${1:-$commentetc}
commentsa=${1:-$commentsa}


# 1st, the standard /etc etckeeper into its own remote git bitbucket:
(cd /etc &&  git add . -A; git diff --staged; git status; git commit -a -v -m "$commentetc; $LOGNAME@$(hostname),$(date +%Y-%m-%d_%H-%M-%S)"; git pull -v; time git push -v -v; (git describe --tags))


# Next, 

# time rsync -azPx --delete  --delete-excluded  --exclude ".git/"  --link-dest=/etc/  /etc/  stage/etc/


#  409  2014-05-05_16-21-14 (cd /root/_repos/sysadmin; rsync -azPx --delete  --delete-excluded  --exclude "0.git/"  --link-dest=/etc/  /etc/  stage/etc/; git add . -A; git diff --staged; git status; git commit -a -v -m "OTHERS-1 #time 60m  #comment fixed damaged filesystem for /srv hosting stagign projects; $USERNAME@$(hostname),$(date +%Y-%m-%d_%H-%M-%S)"; git pull -v; time git push -v -v; (git describe --tags))


(cd /root/_repos/sysadmin; \
cp -avf /srv/*.sh stage/srv/; \
rsync -azPx --delete  --delete-excluded  --exclude "0.git/"  --link-dest=/etc/  /etc/  stage/etc/; \
 git add . -A; git diff --staged; git status; git commit -a -v -m "$commentsa; $LOGNAME@$(hostname),$(date +%Y-%m-%d_%H-%M-%S)"; git pull -v; time git push -v -v; (git describe --tags))


