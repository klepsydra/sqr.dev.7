;
; BIND data file for local loopback interface
;
$TTL    604800
;@       IN      SOA     ns.sqr.dev. robert.linacti.com. (
@       IN      SOA     ns.sqr-dev.pl. robert.linacti.com. (
                     2014082701         ; Serial  YYYYMMDDXX
                         604800         ; Refresh
                          86400         ; Retry
                        2419200         ; Expire
                         604800 )       ; Negative Cache TTL
;
sqr-dev.pl.	IN      NS      ns.sqr-dev.pl.
;sqr-dev.pl.	IN	A	192.168.1.7
;sqr.dev.	IN      NS      ns.sqr-dev.pl.
;sqr.dev.	IN	A	192.168.1.7

; Can this cause problems to outside resolution if NS's A record points to an internal LAN address?
;;ns		IN	A	192.168.1.7   
ns		IN	A	192.168.1.3
ns2		IN	A	192.168.1.7   
xenjux		IN	A	192.168.1.2   

ext		CNAME	sqrdev.55.lt.
pfr		CNAME	sqrdev.55.lt.
; pfr.sqr-dev.pl rorsqr.petfriendlyrestaurants.com sqr.petfriendlyrestaurants.com ror.petfriendlyrestaurants.com

;sqr-dev.pl. IN	CNAME	stage
;Jul 22 12:28:10 stage named[25423]: dns_master_load: /etc/bind/db.sqr-dev.pl:26: sqr-dev.pl: CNAME and other data
;Jul 22 12:28:10 stage named[25423]: zone sqr-dev.pl/IN: loading from master file /etc/bind/db.sqr-dev.pl failed: CNAME and other data
;Jul 22 12:28:10 stage named[25423]: zone sqr-dev.pl/IN: not loaded due to errors.

;@	CNAME	stage.sqr-dev.pl. ; dns_master_load: /etc/bind/db.sqr-dev.pl:35: sqr-dev.pl: CNAME and other data
;;@	IN  A   	192.168.1.7   ; finally accepted A record for bare domain
@	IN  A   	192.168.1.3   ; finally accepted A record for bare domain

vm-stage	IN  A   	192.168.1.3 ; NEW staging/dev server
stage		IN  A   	192.168.1.7 ; staging/dev server

qa			IN  CNAME	vm-stage
;;qa			IN  CNAME	stage
;qa			IN  A   	192.168.1.7 ; QA/testing server
;;dev			IN	CNAME	stage    ; dev.sqr-dev.pl  is an alias to  stage.sqr-dev.pl
;;www			IN	CNAME	stage
;;webdav			IN	CNAME	stage
dev			IN	CNAME	vm-stage    ; dev.sqr-dev.pl  is an alias to  stage.sqr-dev.pl
www			IN	CNAME	vm-stage
webdav			IN	CNAME	vm-stage


;also list other computers
diet		CNAME	dev
dev.diet	CNAME	dev
bike		CNAME	dev
dev.bike	CNAME	dev
audio		CNAME	dev
sport		CNAME	dev
compare		CNAME	dev
trash		CNAME	dev
dev.trash	CNAME	dev
crm		CNAME	dev
stack		CNAME	dev
core-test	CNAME	dev

robertu		IN	A	192.168.1.7
lmiedzinski	IN	A	192.168.1.7
kkubus		IN	A	192.168.1.7
swiatr		IN	A	192.168.1.25
mbaran		IN	A	192.168.1.7
marcos	IN	A	192.168.1.10
gordito	CNAME	marcos
mkobylecki	CNAME	marcos
psmietana	IN	A	192.168.1.7


crm.robertu	IN	A	192.168.1.7

; QA testing
;qa.bike		IN	A	192.168.1.7
qa.bike		CNAME	qa
qa.diet		CNAME	qa
qa.trash	CNAME	qa



; See  whois sqr-dev.pl

;nameservers:           dns108.ovh.net.
;                       ns108.ovh.net.
;created:               2014.04.25 12:47:11
;last modified:         2014.04.28 09:29:08
;renewal date:          2015.04.25 12:47:11


;DOMAIN NAME:           sqr-dev.pl
;registrant type:       organization
;nameservers:           dns108.ovh.net. 
;                       ns108.ovh.net. 
;created:               2014.04.25 12:47:11
;last modified:         2014.06.05 15:50:07
;renewal date:          2015.04.25 12:47:11
;dnssec:                Signed
;DS:                    18535 8 1 ADA64D9679DF4645DF915B0148CE94F62DA7CE22

